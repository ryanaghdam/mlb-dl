const fs = require('fs');
const path = require('path');
const request = require('request');

const DL_API_URL = 'https://fantasybaseballnerd.com/service/injuries/';

function fetchInjuredListFromFbn() {
  return new Promise(function (resolve, reject) {
    request(DL_API_URL, function handleRequest(err, response, body) {
      if (err) {
        return reject(err);
      };

      if (response.statusCode !== 200) {
        return reject(new Error('API returned with status ' + response.statusCode));
      }

      resolve(body);
    });
  });
}

function mockFetchInjuredList() {
  return new Promise(function (resolve, reject) {
    fs.readFile(path.join(__dirname, '../fixtures/injured.xml'), function (err, data) {
      if (err) {
        return reject(err);
      }

      return resolve(data);
    });
  });
}

module.exports = {
  fetchInjuredList: (function getFetchFunctionForEnvironment(env) {
    return env === 'production' ? fetchInjuredListFromFbn : mockFetchInjuredList
  })(process.env.NODE_ENV)
};
