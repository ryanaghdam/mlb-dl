const { getInjuredList } = require('./repository.js');

module.exports = {
  getInjuredList: (function createCachedGetInjuredList(cacheTimeoutInMs) {
    let cachedResponse;
    let timestamp;

    function hasCachedValue() {
      return typeof cachedResponse !== 'undefined';
    }

    function hasCacheExpired() {
      return (new Date() - timestamp) > cacheTimeoutInMs;
    }

    return function cachedGetInjuredList() {
      if (!hasCachedValue() || hasCacheExpired()) {
        console.log('cache empty or expired');
        timestamp = new Date();
        cachedResponse = getInjuredList();
      }

      return cachedResponse;
    }
  })(1000 * 60 * 60 * 6)
};

