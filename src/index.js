const express = require('express');
const path = require('path');
const app = express();
const { getInjuredList } = require('./cachedInjuredListService.js');

app.set('view engine', 'pug');

function renderPage(response) {
  return function (injuredList) {
    return new Promise(function (resolve, reject) {
      response.render('index', { injuredList }, function (err, html) {
        if (err) {
          return reject(err);
        }

        response.send(html);
        resolve();
      });
    });
  };
}

app.get('/', function disabledListController(req, res, next) {
  return getInjuredList()
    .then(renderPage(res))
    .then(next)
    .catch(next);
});

const port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log(`Server started on port ${port}.`);
});
