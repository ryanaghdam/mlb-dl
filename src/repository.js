const { fetchInjuredList } = require('./adapter.js');
const { disabledListFromXml } = require('./domain.js');

function getInjuredList() {
  return fetchInjuredList().then(disabledListFromXml);
}

module.exports = { getInjuredList };
