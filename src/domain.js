const { parseString } = require('xml2js');

function parseXml(rawXmlString) {
  return new Promise(function disabledListFromXmlInner(resolve, reject) {
    parseString(rawXmlString, function handleParseString(err, result) {
      if (err) {
        return reject(err);
      }

      resolve(result);
    });
  });
}

function collectInjuredPlayers(parsedApiResponse) {
  return parsedApiResponse.FantasyBaseballNerd.Team.reduce(function getPlayersFromTeam(acc, team) {
    team.Player.forEach(function getPlayerFromTeam(player) {
      acc.push({
        id: player.playerId[0],
        name: player.playerName[0],
        team: player.team[0],
        position: player.position[0],
        notes: [player.reason.join(', '), player.notes.join('.  ')].join('.  '),
      });
    });
    return acc;
  }, []);
}

function disabledListFromXml(rawXmlString) {
  return parseXml(rawXmlString).then(collectInjuredPlayers);
}

module.exports = { disabledListFromXml };
